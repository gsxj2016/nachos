// threadtest.cc 
//	Simple test case for the threads assignment.
//
//	Create two threads, and have them context switch
//	back and forth between themselves by calling Thread::Yield, 
//	to illustratethe inner workings of the thread system.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "elevatortest.h"
#include "synch.h"

// testnum is set in main.cc
int testnum = 1;

//----------------------------------------------------------------------
// SimpleThread
// 	Loop 5 times, yielding the CPU to another ready thread 
//	each iteration.
//
//	"which" is simply a number identifying the thread, for debugging
//	purposes.
//----------------------------------------------------------------------

void
SimpleThread(int which)
{
    int num;
    
    for (num = 0; num < 5; num++) {
	printf("*** thread %d looped %d times\n", which, num);
        currentThread->Yield();
    }
}

//----------------------------------------------------------------------
// ThreadTest1
// 	Set up a ping-pong between two threads, by forking a thread 
//	to call SimpleThread, and then calling SimpleThread ourselves.
//----------------------------------------------------------------------

void
ThreadTest1()
{
    DEBUG('t', "Entering ThreadTest1");

    Thread *t = new Thread("forked thread");

    t->Fork(SimpleThread, (void*)1);
    SimpleThread(0);
}


void
AnotherSimpleThread(int)
{
    currentThread->Yield();
    printf("........\n");
}

void
YetAnotherSimpleThread(int)
{
    Thread *t = new Thread("thread 3");
    t->Fork(AnotherSimpleThread, NULL);
    currentThread->Yield();
    printf("...\n");
}

void
ThreadTest2()
{
    DEBUG('t', "Entering ThreadTest2");
    Thread *t = new Thread("thread 1");
    Thread *u = new Thread("thread 2");
    Thread *v = new Thread("thread 4", 1000);
    t->Fork(YetAnotherSimpleThread, NULL);
    u->Fork(AnotherSimpleThread, NULL);
    currentThread->Yield();
    ShowThreadStatus();
    printf(".\n");
}


void
ThreadTest3()
{
    for (int i = 0; ++i;) {
        Thread *t = new Thread("t");
        printf("Created %d child threads\n", i);
    }
}

void
SimpleThreadNoLoop(int which)
{
    printf("*** Thread with priority %d run\n", which);
}

void
ThreadPriorityTest1()
{
    int pr[11] = {1, 9, 2, 8, 3, 7, 4, 6, 5, 0, 1};
    for (int i = 0; i < 11; ++i) {
        Thread *t = new Thread("t");
        t->setPriority(pr[i]);
        t->Fork(SimpleThreadNoLoop, (void *)pr[i]);
    }

    ShowThreadStatus();
    printf("########### GO!\n");
}

void
ThreadPriorityTest2()
{
    Thread *t = new Thread("forked thread");
    t->setPriority(-10);
    t->Fork(SimpleThreadNoLoop, (void *)-10);

    SimpleThreadNoLoop(0);
}

void
ThreadPriorityTest3()
{
    printf("##### NOW FORK THREAD -10\n");

    Thread *t = new Thread("forked thread 1");
    t->setPriority(-10);
    t->Fork(SimpleThread, (void*)-10);

    printf("##### NOW FORK THREAD 10\n");

    Thread *u = new Thread("forked thread 2");
    u->setPriority(10);
    u->Fork(SimpleThread, (void*)10);

    printf("##### NOW RUN MAIN THREAD\n");

    SimpleThread(0);
}

void
SimpleThreadWithTicks(int which)
{
    int num;

    for (num = 1; num <= 50; num++) {
        if ((num & 3) == 0)
            printf("*** thread %d looped %d times\n", which, num);
        interrupt->OneTick();
    }
    printf("*** thread %d done\n", which);
}

void
ThreadRRTest1()
{
    Thread *t = new Thread("forked thread");
    t->Fork(SimpleThreadWithTicks, (void*)1);
    SimpleThreadWithTicks(0);
}


//----------------------------------------------------------------------
// Producer & Consumer
//----------------------------------------------------------------------
#define BUFFER_SIZE 3
#define MAX_PRODUCE 7
int *buffer;
int bHead, bTail;
Semaphore *semMutex, *semItem, *semSlot;

void
ProducerS(int which)
{
    for (int i = 0; i < MAX_PRODUCE; ++i) {
        semSlot->P();
        semMutex->P();
        printf("Producer %d produces item %d\n", which, i + which * 1000);
        buffer[bTail] = i + which * 1000;
        bTail = (bTail + 1) % BUFFER_SIZE;
        semMutex->V();
        semItem->V();
    }
}

void
ConsumerS(int which)
{
    for(;;) {
        semItem->P();
        semMutex->P();
        int item = buffer[bHead];
        bHead = (bHead + 1) % BUFFER_SIZE;
        printf("Consumer %d consumes item %d\n", which, item);
        semMutex->V();
        semSlot->V();
    }
}

void
SynchTest1()
{
    buffer = new int[BUFFER_SIZE];
    bHead = bTail = 0;
    semMutex = new Semaphore("mutex", 1);
    semSlot = new Semaphore("slot", BUFFER_SIZE);
    semItem = new Semaphore("item", 0);

    Thread *c1 = new Thread("c1"); c1->Fork(ConsumerS, (void *) 1);
    Thread *p1 = new Thread("p1"); p1->Fork(ProducerS, (void *) 1);
    Thread *p2 = new Thread("p2"); p2->Fork(ProducerS, (void *) 2);
}

Lock *lock;
Condition *slotAvailable, *itemAvailable;
int bCnt;

void
ProducerCV(int which)
{
    lock->Acquire();
    for (int i = 0; i < MAX_PRODUCE; ++i) {
        while (bCnt == BUFFER_SIZE)
            slotAvailable->Wait(lock);
        printf("Producer %d produces item %d\n", which, i + which * 1000);
        buffer[bTail] = i + which * 1000;
        bTail = (bTail + 1) % BUFFER_SIZE;
        ++bCnt;
        itemAvailable->Signal(lock);
    }
    lock->Release();
}

void
ConsumerCV(int which)
{
    lock->Acquire();
    for(;;) {
        while (bCnt == 0)
            itemAvailable->Wait(lock);
        int item = buffer[bHead];
        bHead = (bHead + 1) % BUFFER_SIZE;
        printf("Consumer %d consumes item %d\n", which, item);
        --bCnt;
        slotAvailable->Broadcast(lock);
    }
    lock->Release();
}

void
SynchTest2()
{
    buffer = new int[BUFFER_SIZE];
    bHead = bTail = 0;
    bCnt = 0;
    lock = new Lock("lock");
    slotAvailable = new Condition("slot");
    itemAvailable = new Condition("item");

    Thread *c1 = new Thread("c1"); c1->Fork(ConsumerCV, (void *) 1);
    Thread *p1 = new Thread("p1"); p1->Fork(ProducerCV, (void *) 1);
    Thread *c2 = new Thread("c2"); c2->Fork(ConsumerCV, (void *) 2);
    Thread *p2 = new Thread("p2"); p2->Fork(ProducerCV, (void *) 2);
}

//----------------------------------------------------------------------
// Philosopher
//----------------------------------------------------------------------

#define PHILO_CNT 5
#define EAT_CNT 3
Lock *forkLock[PHILO_CNT];
void Philosopher(int which)
{
    int fork1 = which;
    int fork2 = (which + 1) %PHILO_CNT;
    if (fork1 > fork2) {
        int tmp = fork1;
        fork1 = fork2;
        fork2 = tmp;
    }
    for(int i = 0; i < EAT_CNT; ++i) {
        forkLock[fork1]->Acquire();
        printf("Philosopher %d picked up fork %d\n", which, fork1);
        forkLock[fork2]->Acquire();
        printf("Philosopher %d picked up fork %d\n", which, fork2);
        printf("Philosopher %d eating...\n", which);
        printf("Philosopher %d put down fork %d\n", which, fork2);
        forkLock[fork2]->Release();
        printf("Philosopher %d put down fork %d\n", which, fork1);
        forkLock[fork1]->Release();
    }
}

void SynchTest3()
{
    for(int i = 0; i < PHILO_CNT; ++i)
        forkLock[i] = new Lock("fork");
    for(int i = 0; i < PHILO_CNT; ++i)
        (new Thread("philo"))->Fork(Philosopher, (void *)i);
}

//----------------------------------------------------------------------
// Barrier
//----------------------------------------------------------------------

#define BARRIER_CNT 2
#define THRD_CNT 8
Barrier *barrier[BARRIER_CNT];

void
Runner(int which)
{
    for (int i = 0; i < BARRIER_CNT; ++i) {
        printf("Thread %d reached barrier %d\n", which, i);
        barrier[i]->Reach();
    }
}

void
BarrierTest()
{
    for (int i = 0; i < BARRIER_CNT; ++i)
        barrier[i] = new Barrier("b", THRD_CNT);
    for (int i = 0; i < THRD_CNT; ++i)
        (new Thread("t"))->Fork(Runner, (void *)i);
}


//----------------------------------------------------------------------
//  Rwlock
//----------------------------------------------------------------------
Rwlock *rwl;
int sharedVar;

void
Writer(int which)
{
    rwl->WriteBegin();
    printf("Writer %d begins\n", which);
    currentThread->Yield();
    sharedVar = which;
    printf("Writer %d ends\n", which);
    rwl->WriteEnd();
}

void
Reader(int which)
{
    rwl->ReadBegin();
    printf("Reader %d begins\n", which);
    currentThread->Yield();
    int val = sharedVar;
    printf("Reader %d ends, value is %d\n", which, val);
    rwl->ReadEnd();
}

void
RwlockTest()
{
    sharedVar = 0;
    rwl = new Rwlock("rwlock");

    (new Thread("wr1"))->Fork(Writer, (void *)1);
    (new Thread("rd1"))->Fork(Reader, (void *)1);
    (new Thread("rd2"))->Fork(Reader, (void *)2);
    (new Thread("wr2"))->Fork(Writer, (void *)2);
    (new Thread("rd3"))->Fork(Reader, (void *)3);
    (new Thread("rd4"))->Fork(Reader, (void *)4);
    (new Thread("rd5"))->Fork(Reader, (void *)5);
}

void
TestSuspendThread(int which)
{
    printf("Entered thread 1\n");

    printf("Thread 1 try acquiring lock...\n");
    lock->Acquire();
    printf("Lock acquired! 1\n");
    lock->Release();
    printf("Lock release. Thread 1 yielding...\n"); currentThread->Yield();
}

void
SuspendTest1()
{
    lock = new Lock("lock");
    lock->Acquire();
    printf("Lock acquired! 0\n");

    printf("Spawn thread 1\n");
    Thread *t = new Thread("child");
    t->Fork(TestSuspendThread, (void *) 1);
    printf("Thread 0 yielding...\n"); currentThread->Yield();

    printf("==== Testing BLOCKED => SUSPEND_BLOCK\n");

    printf("Suspend thread 1\n"); t->Suspend();
    printf("Thread 0 yielding... "); currentThread->Yield(); printf("  [Not switching] OK\n");
    printf("Lock release! 0\n"); lock->Release();
    printf("Thread 0 yielding... "); currentThread->Yield(); printf("  [Not switching] OK\n");
    printf("Awake thread 1\n"); t->Awake();
    currentThread->Yield();

    lock->Acquire(); currentThread->Yield();

}

void SuspendTest2() {
    lock = new Lock("lock");
    lock->Acquire();
    printf("Lock acquired! 1\n");

    printf("Spawn thread 1\n");
    Thread *t = new Thread("child");
    t->Fork(TestSuspendThread, (void *) 1);
    printf("Thread 0 yielding...\n"); currentThread->Yield();

    printf("==== Testing BLOCKED <=> SUSPEND_BLOCK\n");
    printf("Suspend thread 1\n"); t->Suspend();
    printf("Thread 0 yielding... "); currentThread->Yield(); printf("  [Not switching] OK\n");
    printf("Awake thread 1\n"); t->Awake();
    printf("Thread 0 yielding... "); currentThread->Yield(); printf("  [Not switching] OK\n");
    printf("Lock release! 0\n"); lock->Release();
    currentThread->Yield();
}

//----------------------------------------------------------------------
// ThreadTest
// 	Invoke a test routine.
//----------------------------------------------------------------------

void
ThreadTest()
{
    switch (testnum) {
    case 1:
	ThreadTest1();
	break;
    case 2:
        ThreadTest2();
        break;
    case 3:
        ThreadTest3();
        break;
    case 4:
        SynchTest1();
        break;
    case 5:
        SynchTest2();
        break;
    case 6:
        SynchTest3();
        break;
    case 7:
        BarrierTest();
        break;
    case 8:
        RwlockTest();
        break;
    case 9:
        SuspendTest1();
        break;
    case 10:
        SuspendTest2();
        break;
    default:
	printf("No test specified.\n");
	break;
    }
}

