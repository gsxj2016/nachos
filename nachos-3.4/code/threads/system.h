// system.h 
//	All global variables used in Nachos are defined here.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef SYSTEM_H
#define SYSTEM_H

#include "copyright.h"
#include "utility.h"
#include "thread.h"
#include "scheduler.h"
#include "interrupt.h"
#include "stats.h"
#include "timer.h"
#ifdef USER_PROGRAM
#include "bitmap.h"
#include "synchconsole.h"
#endif

// Initialization and cleanup routines
extern void Initialize(int argc, char **argv); 	// Initialization,
						// called before anything else
extern void Cleanup();				// Cleanup, called when
						// Nachos is done.
#ifdef USER_PROGRAM
extern void ReplacePage();
#endif

extern Thread *currentThread;			// the thread holding the CPU
extern Thread *threadToBeDestroyed;  		// the thread that just finished
extern Scheduler *scheduler;			// the ready list
extern Interrupt *interrupt;			// interrupt status
extern Statistics *stats;			// performance metrics
extern Timer *timer;				// the hardware alarm clock

#define MAX_THREADS 128
extern Thread *existingThreads[MAX_THREADS];
extern int threadCount;

#ifdef USER_PROGRAM
#ifdef FILESYS
#define SWAP_SIZE 30
#else
#define SWAP_SIZE 2048
#endif
#define SWAP_FILENAME "NACHOS_SWAPFILE"
extern BitMap *pageFrameMap;
extern class VMInfo **pageFrameInfo;
extern BitMap *swapPageMap;
extern OpenFile *swapFile;
extern class SynchConsole *tty;
#endif

#define ALWAYS_CREATE_TIMER


#ifdef USER_PROGRAM
#include "machine.h"
extern Machine* machine;	// user program memory and registers
#endif

#ifdef FILESYS_NEEDED 		// FILESYS or FILESYS_STUB 
#include "filesys.h"
extern FileSystem  *fileSystem;
#endif

#ifdef FILESYS
#include "synchdisk.h"
extern SynchDisk   *synchDisk;
extern OpenFileManager *openFileManager;
#endif

#ifdef NETWORK
#include "post.h"
extern PostOffice* postOffice;
#endif

#endif // SYSTEM_H
