// addrspace.cc 
//	Routines to manage address spaces (executing user programs).
//
//	In order to run a user program, you must:
//
//	1. link with the -N -T 0 option 
//	2. run coff2noff to convert the object file to Nachos format
//		(Nachos object code format is essentially just a simpler
//		version of the UNIX executable object code format)
//	3. load the NOFF file into the Nachos file system
//		(if you haven't implemented the file system yet, you
//		don't need to do this last step)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "addrspace.h"
#include "noff.h"
#include "synch.h"
#ifdef HOST_SPARC
#include <strings.h>
#endif

//----------------------------------------------------------------------
// SwapHeader
// 	Do little endian to big endian conversion on the bytes in the 
//	object file header, in case the file was generated on a little
//	endian machine, and we're now running on a big endian machine.
//----------------------------------------------------------------------

static void 
SwapHeader (NoffHeader *noffH)
{
	noffH->noffMagic = WordToHost(noffH->noffMagic);
	noffH->code.size = WordToHost(noffH->code.size);
	noffH->code.virtualAddr = WordToHost(noffH->code.virtualAddr);
	noffH->code.inFileAddr = WordToHost(noffH->code.inFileAddr);
	noffH->initData.size = WordToHost(noffH->initData.size);
	noffH->initData.virtualAddr = WordToHost(noffH->initData.virtualAddr);
	noffH->initData.inFileAddr = WordToHost(noffH->initData.inFileAddr);
	noffH->uninitData.size = WordToHost(noffH->uninitData.size);
	noffH->uninitData.virtualAddr = WordToHost(noffH->uninitData.virtualAddr);
	noffH->uninitData.inFileAddr = WordToHost(noffH->uninitData.inFileAddr);
}

//----------------------------------------------------------------------
// AddrSpace::AddrSpace
// 	Create an address space to run a user program.
//	Load the program from a file "executable", and set everything
//	up so that we can start executing user instructions.
//
//	Assumes that the object code file is in NOFF format.
//
//	First, set up the translation from program memory to physical 
//	memory.  For now, this is really simple (1:1), since we are
//	only uniprogramming, and we have a single unsegmented page table
//
//	"executable" is the file containing the object code to load into memory
//----------------------------------------------------------------------

AddrSpace::AddrSpace(OpenFile *executable)
{
    NoffHeader noffH;
    unsigned int i, size;

    executable->ReadAt((char *)&noffH, sizeof(noffH), 0);
    if ((noffH.noffMagic != NOFFMAGIC) && 
		(WordToHost(noffH.noffMagic) == NOFFMAGIC))
    	SwapHeader(&noffH);
    ASSERT(noffH.noffMagic == NOFFMAGIC);

// how big is address space?
    size = noffH.code.size + noffH.initData.size + noffH.uninitData.size 
			+ UserStackSize;	// we need to increase the size
						// to leave room for the stack
    numPages = divRoundUp(size, PageSize);
    size = numPages * PageSize;

    DEBUG('a', "Initializing address space, num pages %d, size %d\n", 
					numPages, size);

// first, set up the translation 
    pageTable = new TranslationEntry[numPages];
    vmMap = new VMInfo[numPages];
    for (i = 0; i < numPages; i++) {
        // not map now
        pageTable[i].virtualPage = i;
        pageTable[i].valid = FALSE;
        // set page info
        vmMap[i].space = this;
        vmMap[i].readOnly = (i + 1) * PageSize <= noffH.code.size;
        vmMap[i].swapPageId = -1;
        vmMap[i].pte = NULL;
    }

// prepare for lazy loading
    exec = executable;
    codeVirtAddr = noffH.code.virtualAddr;
    codeFileAddr = noffH.code.inFileAddr;
    codeSize = noffH.code.size;
    dataVirtAddr = noffH.initData.virtualAddr;
    dataFileAddr = noffH.initData.inFileAddr;
    dataSize = noffH.initData.size;
    if (dataSize == 0) {
        dataVirtAddr = 0;
    }
    ASSERT(codeSize > 0);
    ASSERT(codeVirtAddr == 0);
    DEBUG('a', "Code size %d, start from 0x%x\n", codeSize, codeVirtAddr);
    DEBUG('a', "Data size %d, start from 0x%x\n", dataSize, dataVirtAddr);

    fd = new OpenFile*[NumMaxOpenFile];
    for (int i = 0; i < NumMaxOpenFile; ++i) {
        fd[i] = NULL;
    }

    child = new int[NumMaxExecChild];
    for (int i = 0; i < NumMaxExecChild; ++i) {
        child[i] = -1;
    }
    reap = new Semaphore("reap sem", 0);
    wxit = new Semaphore("wait exit sem", 0);
}

//----------------------------------------------------------------------
// AddrSpace::~AddrSpace
// 	Dealloate an address space.
//----------------------------------------------------------------------

AddrSpace::~AddrSpace()
{
    ASSERT(machine->pageTable == pageTable);
    for (int i = 0; i < numPages; ++i) {
        if (vmMap[i].pte != NULL) {
            UnloadPage(i, TRUE);
        } else {
            ForceUnloadSwap(i);
        }
        ASSERT(!pageTable[i].valid);
    }
    machine->pageTable = NULL;
    delete [] pageTable;
    delete [] vmMap;
    delete exec;

    if (machine->tlb != NULL) {
        for(int i = 0; i < TLBSize; ++i) {
            ASSERT(!machine->tlb[i].valid);
        }
    }

    for(int i = 0; i < NumMaxOpenFile; ++i) {
        if (fd[i] != NULL)
            delete fd[i];
    }
    delete [] fd;


    delete [] child;
    delete reap;
    delete wxit;
}

//----------------------------------------------------------------------
// AddrSpace::InitRegisters
// 	Set the initial values for the user-level register set.
//
// 	We write these directly into the "machine" registers, so
//	that we can immediately jump to user code.  Note that these
//	will be saved/restored into the currentThread->userRegisters
//	when this thread is context switched out.
//----------------------------------------------------------------------

void
AddrSpace::InitRegisters()
{
    int i;

    for (i = 0; i < NumTotalRegs; i++)
	machine->WriteRegister(i, 0);

    // Initial program counter -- must be location of "Start"
    machine->WriteRegister(PCReg, 0);	

    // Need to also tell MIPS where next instruction is, because
    // of branch delay possibility
    machine->WriteRegister(NextPCReg, 4);

   // Set the stack register to the end of the address space, where we
   // allocated the stack; but subtract off a bit, to make sure we don't
   // accidentally reference off the end!
    machine->WriteRegister(StackReg, nextStack = (numPages * PageSize - 16));
    DEBUG('a', "Initializing stack register to %d\n", numPages * PageSize - 16);
}

//----------------------------------------------------------------------
// AddrSpace::SaveState
// 	On a context switch, save any machine state, specific
//	to this address space, that needs saving.
//
//	For now, nothing!
//----------------------------------------------------------------------

void AddrSpace::SaveState() 
{}

//----------------------------------------------------------------------
// AddrSpace::RestoreState
// 	On a context switch, restore the machine state so that
//	this address space can run.
//
//      For now, tell the machine where to find the page table.
//----------------------------------------------------------------------

void AddrSpace::RestoreState() 
{
    machine->FlushTLB();
    machine->pageTable = pageTable;
    machine->pageTableSize = numPages;
}

bool AddrSpace::LoadPageAt(int virtualAddr)
{
    int vpn = virtualAddr / PageSize;
    int offset = virtualAddr % PageSize;

    ASSERT(vpn >= 0 && vpn < numPages);
    ASSERT(machine->pageTable == pageTable);

    TranslationEntry *pte = &pageTable[vpn];
    VMInfo *info = &vmMap[vpn];
    
    ASSERT(!pte->valid);
    ASSERT(info->pte == NULL);
    
    // Load page
    int ppn = pageFrameMap->Find();
    if (ppn == -1) {
        DEBUG('a', "Requesting page replacement...\n");
        if(NULL == swapPageMap || 0 == swapPageMap->NumClear()) {
            DEBUG('a', "*** NO SWAP SPACE LEFT\n");
            return FALSE;
        }
        ReplacePage();
        ppn = pageFrameMap->Find();
        ASSERT(ppn != -1);
    }

    pte->virtualPage = vpn;
    pte->physicalPage = ppn;
    pte->readOnly = info->readOnly;
    pte->use = FALSE;
    pte->dirty = FALSE;

    int pa = ppn * PageSize;

    if (info->swapPageId == -1) { // demand zero page OR load from file
        DEBUG('a', "Loading page %d (frame %d)\n", vpn, ppn);
        bzero(&machine->mainMemory[pa], PageSize);

        // load segments from executable file
        LoadSegmentToPage(vpn, exec, codeSize, codeVirtAddr, codeFileAddr);
        LoadSegmentToPage(vpn, exec, dataSize, dataVirtAddr, dataFileAddr);
        
    } else { // load from swap
        DEBUG('a', "Loading page %d (frame %d): from swap (Id %d)\n", vpn, ppn, info->swapPageId);
        swapFile->ReadAt(&(machine->mainMemory[pa]), PageSize, info->swapPageId * PageSize);
        swapPageMap->Clear(info->swapPageId);
        info->swapPageId = -1;
        pte->dirty = TRUE;
    }

    pte->valid = TRUE;
    info->pte = pte;
    pageFrameInfo[ppn] = info;
    return TRUE;
}

void AddrSpace::LoadSegmentToPage(int vpn, OpenFile *file, int segSize, int virtStart, int fileStart) {
    ASSERT(vpn >= 0 && vpn < numPages);
    ASSERT(pageTable == machine->pageTable);

    int vpa = vpn * PageSize;
    int addr = vpa;
    if (addr + PageSize > virtStart && addr < virtStart + segSize) {
        addr = max(addr, virtStart);
        int prem = vpa + PageSize - addr;
        int len = virtStart + segSize - addr;
        len = min(len, prem);
        int fa = fileStart + addr - virtStart;
        int pa = pageTable[vpn].physicalPage * PageSize + (addr - vpa);

        ASSERT(addr - vpa < PageSize);
        ASSERT(addr - vpa + len <= PageSize);
        ASSERT(addr - virtStart + len <= segSize);
        DEBUG('a', "  Loading from file [file start: %x,  virt start: %x, len: %x]\n", fa, addr, len);
        file->ReadAt(&(machine->mainMemory[pa]), len, fa);
    }
}

void AddrSpace::ForceUnloadSwap(int vpn)
{
    ASSERT(vpn >= 0 && vpn < numPages);
    ASSERT(machine->pageTable == pageTable);

    TranslationEntry *pte = &pageTable[vpn];
    VMInfo *info = &vmMap[vpn];

    ASSERT(!pte->valid);
    ASSERT(info->pte == NULL);

    if (info->swapPageId != -1) {
        DEBUG('a', "Purging swap page for virtual page %d (swap page %d)\n", vpn, info->swapPageId);
        swapPageMap->Clear(info->swapPageId);
        info->swapPageId = -1;
    }
}

void AddrSpace::UnloadPage(int vpn, bool forceUnload)
{
    ASSERT(vpn >= 0 && vpn < numPages);
    ASSERT(machine->pageTable == pageTable);

    TranslationEntry *pte = &pageTable[vpn];
    VMInfo *info = &vmMap[vpn];

    ASSERT(pte->valid);
    ASSERT(info->pte == pte);

    // write back PTE from TLB
    machine->FindAndEvictTLBEntry(vpn);

    int pa = pte->physicalPage * PageSize;

    DEBUG('a', "Unloading page %d: ", vpn);
    if (pte->dirty && !forceUnload) {
        ASSERT(swapPageMap != NULL);
        int sp = swapPageMap->Find();
        ASSERT(sp != -1);

        DEBUG('a', "swap (Id %d)\n", sp);

        info->swapPageId = sp;
        swapFile->WriteAt(&(machine->mainMemory[pa]), PageSize, sp * PageSize);
    } else {
        // clean page, no need to swap
        info->swapPageId = -1;
        DEBUG('a', "%s\n", forceUnload ? "***FORCED UNLOAD" : "clean");
    }

    pageFrameMap->Clear(pte->physicalPage);
    pageFrameInfo[pte->physicalPage] = NULL;
    pte->valid = FALSE;
    info->pte = NULL;
}

int AddrSpace::CountDirtyPages()
{
    int cnt = 0;
    for (int i = 0; i < numPages; ++i) {
        if (pageTable[i].valid && pageTable[i].dirty) {
            ++cnt;
        }
    }
    return cnt;
}

void AddrSpace::SwapAllPages()
{
    ASSERT(CountDirtyPages() <= swapPageMap->NumClear());
    for (int i = 0; i < numPages; ++i) {
        if (pageTable[i].valid)
            UnloadPage(i, FALSE);
    }
}

bool AddrSpace::WriteToAddrSpace(int va, char *buffer, int size)
{
    if (va < 0) return FALSE;
    int pageStart = va / PageSize;
    int pageLast = (va + size - 1) / PageSize;
    for (int i = pageStart; i <= pageLast; ++i) {
        if (i >= numPages) return FALSE;
        if (vmMap[i].readOnly) return FALSE;
        if (vmMap[i].pte == NULL) {
            if (!LoadPageAt(i * PageSize)) return FALSE;
        }
        int offset = va - i * PageSize;
        int len = min(PageSize - offset, size);
        memcpy(machine->mainMemory + vmMap[i].pte->physicalPage * PageSize + offset,
                buffer, size);
        size -= len, va += len, buffer += len;
        if(size <= 0) break;
    }
    ASSERT(size == 0);
    return TRUE;
}

bool AddrSpace::ReadFromAddrSpace(int va, char *buffer, int size)
{
    if (va < 0) return FALSE;
    int pageStart = va / PageSize;
    int pageLast = (va + size - 1) / PageSize;
    for (int i = pageStart; i <= pageLast; ++i) {
        if (i >= numPages) return FALSE;
        if (vmMap[i].pte == NULL) {
            if (!LoadPageAt(i * PageSize)) return FALSE;
        }
        int offset = va - i * PageSize;
        int len = min(PageSize - offset, size);
        memcpy(buffer,
                machine->mainMemory + vmMap[i].pte->physicalPage * PageSize + offset,
                size);
        size -= len, va += len, buffer += len;
        if(size <= 0) break;
    }
    ASSERT(size == 0);
    return TRUE;
}

char *AddrSpace::ReadString(int startva) {
    if (startva < 0) return NULL;
    int len = 0;
    for (int addr = startva; ; ++addr) {
        int vpn = addr / PageSize;
        if (vpn >= numPages) return NULL;
        if (vmMap[vpn].pte == NULL) {
            if (!LoadPageAt(vpn * PageSize)) return NULL;
        }
        int offset = addr - vpn * PageSize;
        char now = machine->mainMemory[vmMap[vpn].pte->physicalPage * PageSize + offset];
        ++len;
        if (now == '\0') break;
    }
    char *result = new char[len];
    ReadFromAddrSpace(startva, result, len);
    return result;
}

OpenFile *AddrSpace::GetFd(int id) { return fd[id]; }
void AddrSpace::SetFd(int id, OpenFile *of) { fd[id] = of; }

bool AddrSpace::HasChild(int tid) {
    for(int i=0; i < NumMaxExecChild; ++i)
        if(child[i]==tid)return TRUE;
    return FALSE;
}

void AddrSpace::AppendChild(int tid) {
    for(int i=0; i < NumMaxExecChild; ++i)
        if(child[i]==-1){ child[i]=tid; return; }
    ASSERT(FALSE);
}

void AddrSpace::RemoveChild(int tid) {
    for(int i=0; i < NumMaxExecChild; ++i)
        if(child[i]==tid){ child[i]=-1; return; }
    ASSERT(FALSE);
}

int AddrSpace::Join() { wxit->P(); reap->V(); return exitCode; }
void AddrSpace::Detach() { reap->V(); }
void AddrSpace::SetExit(int ec) {
    exitCode = ec;
    wxit->V(); reap->P();
}

void AddrSpace::DetachChilds() {
    for (int i = 0; i < NumMaxExecChild; ++i) {
        if (child[i] != -1) {
            existingThreads[child[i]]->space->Detach();
        }
    }
}

void AddrSpace::SetMasterThread(int tid) {
    masterThread = tid;
    subCount = 1;
}

void AddrSpace::AddSubThread() {
    ++subCount;
}

void AddrSpace::SubThreadEnd(int tid) {
    if (tid == masterThread) masterThread = -100;
    --subCount; ASSERT(subCount >= 0);
    if (subCount == 0) delete this;
}

bool AddrSpace::IsMasterThread(int tid) {
    return tid == masterThread;
}

int AddrSpace::AllocateStack() {
    return nextStack -= SingleUserStackSize;
}
