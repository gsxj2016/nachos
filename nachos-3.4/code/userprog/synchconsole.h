// synchconsole.h

#ifndef SYNCHCONSOLE_H
#define SYNCHCONSOLE_H

#include "console.h"
#include "synch.h"

class SynchConsole {
  public:
    SynchConsole(char *in, char *out);
    ~SynchConsole();
    char Get();
    void Put(char ch);
    void ReadAvail();
    void WriteDone();

  private:
    Console *console;
    Semaphore *read, *write;
    Lock *lock;
};

#endif // SYNCHCONSOLE_H
