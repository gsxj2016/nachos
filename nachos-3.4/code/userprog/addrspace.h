// addrspace.h 
//	Data structures to keep track of executing user programs 
//	(address spaces).
//
//	For now, we don't keep any information about address spaces.
//	The user level CPU state is saved and restored in the thread
//	executing the user program (see thread.h).
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#ifndef ADDRSPACE_H
#define ADDRSPACE_H

#include "copyright.h"
#include "filesys.h"
#include "translate.h"

#define UserStackSize		4096 	// increase this as necessary!
#define SingleUserStackSize     1024
#define NumMaxOpenFile 16
#define NumMaxExecChild 16

class AddrSpace {
  public:
    AddrSpace(OpenFile *executable);	// Create an address space,
					// initializing it with the program
					// stored in the file "executable"
    ~AddrSpace();			// De-allocate an address space

    void InitRegisters();		// Initialize user-level CPU registers,
					// before jumping to user code

    void SaveState();			// Save/restore address space-specific
    void RestoreState();		// info on a context switch 

    bool LoadPageAt(int virtualAddr);
    void LoadSegmentToPage(int vpn, OpenFile *file, int size, int virtStart, int fileStart);
    void UnloadPage(int vpn, bool forceUnload);
    void ForceUnloadSwap(int vpn);

    int CountDirtyPages();
    void SwapAllPages();

    bool WriteToAddrSpace(int va, char *buffer, int size);
    bool ReadFromAddrSpace(int va, char *buffer, int size);
    char *ReadString(int va);

    OpenFile *GetFd(int id);
    void SetFd(int id, OpenFile *of);

    bool HasChild(int tid);
    void AppendChild(int tid);
    void RemoveChild(int tid);
    int Join();
    void Detach();
    void SetExit(int ec);
    void DetachChilds();

    void SetMasterThread(int tid);
    void AddSubThread();
    void SubThreadEnd(int tid);
    bool IsMasterThread(int tid);
    int AllocateStack();

  private:
    TranslationEntry *pageTable;	// Assume linear page table translation
					// for now!
    unsigned int numPages;		// Number of pages in the virtual 
					// address space
    class VMInfo *vmMap;

    OpenFile *exec;
    int codeSize, codeVirtAddr, codeFileAddr;
    int dataSize, dataVirtAddr, dataFileAddr;

    OpenFile **fd;
    int *child;
    int exitCode;
    int masterThread;
    int subCount;
    int nextStack;

    class Semaphore *reap;
    class Semaphore *wxit;
};

class VMInfo {
    public:
        AddrSpace *space;
        TranslationEntry *pte;
        int swapPageId;
        bool readOnly;
};

#endif // ADDRSPACE_H
