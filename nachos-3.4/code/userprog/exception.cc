// exception.cc 
//	Entry point into the Nachos kernel from user programs.
//	There are two kinds of things that can cause control to
//	transfer back to here from user code:
//
//	syscall -- The user code explicitly requests to call a procedure
//	in the Nachos kernel.  Right now, the only function we support is
//	"Halt".
//
//	exceptions -- The user code does something that the CPU can't handle.
//	For instance, accessing memory that doesn't exist, arithmetic errors,
//	etc.  
//
//	Interrupts (which can also cause control to transfer from user
//	code into the Nachos kernel) are handled elsewhere.
//
// For now, this only handles the Halt() system call.
// Everything else core dumps.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "syscall.h"

//----------------------------------------------------------------------
// ExceptionHandler
// 	Entry point into the Nachos kernel.  Called when a user program
//	is executing, and either does a syscall, or generates an addressing
//	or arithmetic exception.
//
// 	For system calls, the following is the calling convention:
//
// 	system call code -- r2
//		arg1 -- r4
//		arg2 -- r5
//		arg3 -- r6
//		arg4 -- r7
//
//	The result of the system call, if any, must be put back into r2. 
//
// And don't forget to increment the pc before returning. (Or else you'll
// loop making the same system call forever!
//
//	"which" is the kind of exception.  The list of possible exceptions 
//	are in machine.h.
//----------------------------------------------------------------------

static void
AdvancePC()
{
    int pc = machine->ReadRegister(PCReg);
    int npc = machine->ReadRegister(NextPCReg);
    machine->WriteRegister(PrevPCReg, pc);
    machine->WriteRegister(PCReg, npc);
    machine->WriteRegister(NextPCReg, npc+4);
}

static void
ExitThread(int exitCode)
{
    ASSERT(currentThread != NULL);
    if (currentThread->space->IsMasterThread(currentThread->getTid())) {
        currentThread->space->DetachChilds();
        currentThread->space->SetExit(exitCode);
        printf("** Thread %s exited (code: %d)\n", currentThread->getName(), exitCode);
    } else {
        printf("** Child Thread %s exited (code: %d)\n", currentThread->getName(), exitCode);
    }
    currentThread->Finish();
}

static void
KillThread()
{
    ASSERT(currentThread != NULL);
    printf("** KILLING Thread %s...\n", currentThread->getName());
    ExitThread(-1);
}

static void
PageFaultHandler(int virtAddr)
{
    stats->numPageFaults++;
    ASSERT(currentThread != NULL);
    ASSERT(currentThread->space != NULL);
    if (virtAddr / PageSize >= machine->pageTableSize) {
        DEBUG('a', "Thread %d caused segmentation fault, kill it...\n", currentThread->getTid());
        KillThread();
    }
    if (!currentThread->space->LoadPageAt(virtAddr)) {
        DEBUG('a', "Thread %d requested too much pages, kill it...\n", currentThread->getTid());
        KillThread();
    }
}

static void
TLBMissHandler(int virtAddr)
{
    stats->numTLBMisses++;
    TranslationEntry *entry;

    // translate using page table
    entry = machine->GetPageTableEntry(virtAddr);

    if (NULL == entry) {
        // real page fault
        PageFaultHandler(virtAddr);
    } else {
        machine->PutEntryIntoTLB(entry);
    }

}

static void
SysCreate() {
    AdvancePC();
    int nameAddr = machine->ReadRegister(4);
    char *name = currentThread->space->ReadString(nameAddr);
    if (name == NULL) {
        KillThread();
    } else {
        fileSystem->Create(name, 0);
        delete [] name;
    }
}

static void
SysOpen() {
    AdvancePC();
    int nameAddr = machine->ReadRegister(4);
    AddrSpace *space = currentThread->space;
    char *name = space->ReadString(nameAddr);
    if (name == NULL) {
        KillThread();
    } else {
        for (int i = 2; i < NumMaxOpenFile; ++i) {
            if (space->GetFd(i) == NULL) {
                OpenFile *of = fileSystem->Open(name);
                if (of == NULL) break;
                space->SetFd(i, of);
                machine->WriteRegister(2, i);
                return;
            }
        }
        machine->WriteRegister(2, -1);
    }
}

static void
SysClose() {
    AdvancePC();
    int id = machine->ReadRegister(4);
    if (id < 2) return;
    AddrSpace *space = currentThread->space;
    OpenFile *of = space->GetFd(id);
    if (of != NULL) {
        delete of;
        space->SetFd(id, NULL);
    }
}

static void
SysWrite() {
    AdvancePC();
    int ba = machine->ReadRegister(4);
    int len = machine->ReadRegister(5);
    int id = machine->ReadRegister(6);
    AddrSpace *space = currentThread->space;
    if (len <= 0) return;
    if (id < 2) {
        if (id != ConsoleOutput) return;
        if (tty == NULL) return;
        while (len > 0) {
            char ch;
            if (!space->ReadFromAddrSpace(ba, &ch, 1)) KillThread();
            tty->Put(ch);
            --len, ++ba;
        }
        return;
    }

    char *buf = new char[len];
    if (!space->ReadFromAddrSpace(ba, buf, len)) {
        delete [] buf;
        KillThread();
    }
    OpenFile *of = space->GetFd(id);
    if (of != NULL) of->Write(buf, len);
    delete [] buf;
}

static void
SysRead() {
    AdvancePC();
    int ba = machine->ReadRegister(4);
    int len = machine->ReadRegister(5);
    int id = machine->ReadRegister(6);
    AddrSpace *space = currentThread->space;
    if (len <= 0) {
        machine->WriteRegister(2, 0);
        return;
    }
    if (id < 2) {
        if (id != ConsoleInput || tty == NULL) {
            machine->WriteRegister(2, 0);
            return;
        }
        machine->WriteRegister(2, len);
        while (len > 0) {
            char ch = tty->Get();
            if (!space->WriteToAddrSpace(ba, &ch, 1)) KillThread();
            --len, ++ba;
        }
        return;
    }

    OpenFile *of = space->GetFd(id);
    if (of != NULL) {
        char *buf = new char[len];
        int rdlen = of->Read(buf, len);
        if (rdlen > 0) {
            if (!space->WriteToAddrSpace(ba, buf, rdlen)) {
                delete [] buf;
                KillThread();
            }
        }
        delete [] buf;
        machine->WriteRegister(2, rdlen);
    } else {
        machine->WriteRegister(2, 0);
    }
}

static void
SysJoin() {
    AdvancePC();
    AddrSpace *space = currentThread->space;
    int tid = machine->ReadRegister(4);
    if (!space->HasChild(tid)) {
        machine->WriteRegister(2, -1);
    } else {
        int ec = existingThreads[tid]->space->Join();
        space->RemoveChild(tid);
        machine->WriteRegister(2, ec);
    }
}

static void
ExecNewThread(int) {
    currentThread->space->InitRegisters();
    currentThread->space->RestoreState();
    machine->Run();
    ASSERT(FALSE);
}

static void
SysExec() {
    AdvancePC();
    int nameAddr = machine->ReadRegister(4);
    AddrSpace *space = currentThread->space;
    if (!space->HasChild(-1) || threadCount == MAX_THREADS) {
        machine->WriteRegister(2, -1);
        return;
    }
    char *name = space->ReadString(nameAddr);
    if (name == NULL) KillThread();
    OpenFile *executable = fileSystem->Open(name);
    if (executable == NULL) {
        machine->WriteRegister(2, -1);
        delete [] name;
        return;
    }
    AddrSpace *newSpace = new AddrSpace(executable);
    Thread *t = new Thread("exec child");
    t->space = newSpace;
    int tid = t->getTid();
    t->space->SetMasterThread(tid);
    space->AppendChild(tid);
    machine->WriteRegister(2, tid);
    t->Fork(ExecNewThread, NULL);
    delete [] name;
}

static void
ForkedThread(int pc) {
    machine->WriteRegister(PCReg, pc);
    machine->WriteRegister(NextPCReg, pc+4);
    machine->WriteRegister(StackReg,
            currentThread->space->AllocateStack());
    // set ret address to _start+0x4
    machine->WriteRegister(31, 4);
    machine->Run();
    ASSERT(false);
}

static void
SysFork() {
    AdvancePC();
    int funcPtr = machine->ReadRegister(4);
    AddrSpace *space = currentThread->space;
    if (threadCount == MAX_THREADS) { return; }
    Thread *t = new Thread("fork child");
    space->AddSubThread();
    t->space = space;
    t->Fork(ForkedThread, (void *)funcPtr);
}

static void
SysYield() {
    AdvancePC();
    currentThread->Yield();
}

void
ExceptionHandler(ExceptionType which)
{
    int type = machine->ReadRegister(2);

    if (which == PageFaultException) {
        int virtAddr = machine->ReadRegister(BadVAddrReg);
        if (machine->tlb == NULL) {
            PageFaultHandler(virtAddr);
        } else {
            TLBMissHandler(virtAddr);
        }
    } else if (which == ReadOnlyException) {
        KillThread();
    } else if (which == IllegalInstrException) {
        KillThread();
    } else if (which == BusErrorException) {
        KillThread();
    } else if (which == AddressErrorException) {
        KillThread();
    } else if (which == OverflowException) {
        KillThread();
    } else if (which == SyscallException) {
        if (type == SC_Halt) {
            DEBUG('a', "Shutdown, initiated by user program.\n");
            interrupt->Halt();
        } else if (type == SC_Open) {
            SysOpen();
        } else if (type == SC_Close) {
            SysClose();
        } else if (type == SC_Create) {
            SysCreate();
        } else if (type == SC_Read) {
            SysRead();
        } else if (type == SC_Write) {
            SysWrite();
        } else if (type == SC_Join) {
            SysJoin();
        } else if (type == SC_Exec) {
            SysExec();
        } else if (type == SC_Fork) {
            SysFork();
        } else if (type == SC_Yield) {
            SysYield();
        } else if (type == SC_Exit) {
            int exitCode = machine->ReadRegister(4);
            ExitThread(exitCode);
        }
    } else {
	printf("Unexpected user mode exception %d %d\n", which, type);
	ASSERT(FALSE);
    }
}
