// synchconsole.cc

#include "synchconsole.h"

static void ConsoleReadAvail(int arg) { ((SynchConsole*)arg)->ReadAvail(); }
static void ConsoleWriteDone(int arg) { ((SynchConsole*)arg)->WriteDone(); }

SynchConsole::SynchConsole(char *in, char *out)
{
    lock = new Lock("synch console lock");
    read = new Semaphore("synch console read", 0);
    write = new Semaphore("synch console write", 0);
    console = new Console(in, out, ConsoleReadAvail, ConsoleWriteDone, (int)this);
}

SynchConsole::~SynchConsole()
{
    delete console;
    delete lock;
    delete read;
    delete write;
}

char
SynchConsole::Get()
{
    lock->Acquire();
    read->P();
    char ch = console->GetChar();
    lock->Release();
    return ch;
}

void
SynchConsole::Put(char ch)
{
    lock->Acquire();
    console->PutChar(ch);
    write->P();
    lock->Release();
}

void SynchConsole::ReadAvail() { read->V(); }
void SynchConsole::WriteDone() { write->V(); }
