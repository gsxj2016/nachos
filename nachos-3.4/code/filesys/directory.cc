// directory.cc 
//	Routines to manage a directory of file names.
//
//	The directory is a table of fixed length entries; each
//	entry represents a single file, and contains the file name,
//	and the location of the file header on disk.  The fixed size
//	of each directory entry means that we have the restriction
//	of a fixed maximum size for file names.
//
//	The constructor initializes an empty directory of a certain size;
//	we use ReadFrom/WriteBack to fetch the contents of the directory
//	from disk, and to write back any modifications back to disk.
//
//	Also, this implementation has the restriction that the size
//	of the directory cannot expand.  In other words, once all the
//	entries in the directory are used, no more files can be created.
//	Fixing this is one of the parts to the assignment.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "filehdr.h"
#include "directory.h"

//----------------------------------------------------------------------
// Directory::Directory
// 	Initialize a directory; initially, the directory is completely
//	empty.  If the disk is being formatted, an empty directory
//	is all we need, but otherwise, we need to call FetchFrom in order
//	to initialize it from disk.
//
//	"size" is the number of entries in the directory
//----------------------------------------------------------------------

#define InitialTableSize 4
#define InitialPoolSize 20

Directory::Directory()
{
    // initial table
    table = new DirectoryEntry[tableSize = InitialTableSize];
    for (int i = 0; i < tableSize; i++)
	table[i].inUse = FALSE;
    // initial pool
    namePool = new char[namePoolSize = InitialPoolSize];
    namePoolFree = 0;
}

//----------------------------------------------------------------------
// Directory::~Directory
// 	De-allocate directory data structure.
//----------------------------------------------------------------------

Directory::~Directory()
{ 
    delete [] table;
    delete [] namePool;
} 

//----------------------------------------------------------------------
// Directory::FetchFrom
// 	Read the contents of the directory from disk.
//
//	"file" -- file containing the directory contents
//----------------------------------------------------------------------

void
Directory::FetchFrom(OpenFile *file)
{
    file->Seek(0);
    // fetch table size
    (void) file->Read((char *)&(this->tableSize), sizeof(int));
    // fetch table
    delete [] table;
    table = new DirectoryEntry[tableSize];
    (void) file->Read((char *)table, tableSize * sizeof(DirectoryEntry));
    // fetch name pool size
    (void) file->Read((char *)&(this->namePoolSize), sizeof(int));
    (void) file->Read((char *)&(this->namePoolFree), sizeof(int));
    // fetch name pool
    delete [] namePool;
    namePool = new char[namePoolSize];
    (void) file->Read((char *)namePool, namePoolSize);
}

//----------------------------------------------------------------------
// Directory::WriteBack
// 	Write any modifications to the directory back to disk
//
//	"file" -- file to contain the new directory contents
//----------------------------------------------------------------------

void
Directory::WriteBack(OpenFile *file)
{
    ShrinkDirectory();
    file->Seek(0);
    (void) file->Write((char *)&(this->tableSize), sizeof(int));
    (void) file->Write((char *)table, tableSize * sizeof(DirectoryEntry));
    (void) file->Write((char *)&(this->namePoolSize), sizeof(int));
    (void) file->Write((char *)&(this->namePoolFree), sizeof(int));
    (void) file->Write((char *)namePool, namePoolSize);
}

//----------------------------------------------------------------------
// Directory::FindIndex
// 	Look up file name in directory, and return its location in the table of
//	directory entries.  Return -1 if the name isn't in the directory.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

int
Directory::FindIndex(char *name)
{
    for (int i = 0; i < tableSize; i++)
        if (table[i].inUse && !strcmp(namePool + table[i].nameOffset, name))
	    return i;
    return -1;		// name not in directory
}

//----------------------------------------------------------------------
// Directory::Find
// 	Look up file name in directory, and return the disk sector number
//	where the file's header is stored. Return -1 if the name isn't 
//	in the directory.
//
//	"name" -- the file name to look up
//----------------------------------------------------------------------

int
Directory::Find(char *name)
{
    int i = FindIndex(name);

    if (i != -1)
	return table[i].sector;
    return -1;
}

//----------------------------------------------------------------------
// Directory::Add
// 	Add a file into the directory.  Return TRUE if successful;
//	return FALSE if the file name is already in the directory, or if
//	the directory is completely full, and has no more space for
//	additional file names.
//
//	"name" -- the name of the file being added
//	"newSector" -- the disk sector containing the added file's header
//----------------------------------------------------------------------

bool
Directory::Add(char *name, int newSector)
{ 
    if (FindIndex(name) != -1)
	return FALSE;

    for(;;) {
        for (int i = 0; i < tableSize; i++)
            if (!table[i].inUse) {
                table[i].inUse = TRUE;
                table[i].nameOffset = AppendName(name);
                table[i].sector = newSector;
            return TRUE;
            }
        ExpandTable();
    }
}

//----------------------------------------------------------------------
// Directory::Remove
// 	Remove a file name from the directory.  Return TRUE if successful;
//	return FALSE if the file isn't in the directory. 
//
//	"name" -- the file name to be removed
//----------------------------------------------------------------------

bool
Directory::Remove(char *name)
{ 
    int i = FindIndex(name);

    if (i == -1)
	return FALSE; 		// name not in directory
    table[i].inUse = FALSE;
    return TRUE;	
}

//----------------------------------------------------------------------
// Directory::List
// 	List all the file names in the directory. 
//----------------------------------------------------------------------

void
Directory::List()
{
   for (int i = 0; i < tableSize; i++)
	if (table[i].inUse)
	    printf("%s\n", namePool + table[i].nameOffset);
}

//----------------------------------------------------------------------
// Directory::Print
// 	List all the file names in the directory, their FileHeader locations,
//	and the contents of each file.  For debugging.
//----------------------------------------------------------------------

void
Directory::Print()
{ 
    FileHeader *hdr = new FileHeader;

    printf("Directory contents:\n");
    for (int i = 0; i < tableSize; i++)
	if (table[i].inUse) {
	    printf("Name: %s, Sector: %d\n", namePool + table[i].nameOffset, table[i].sector);
	    hdr->FetchFrom(table[i].sector);
	    hdr->Print();
            if (hdr->GetFileType() == DIRECTORY_FILE) {
                OpenFile *openFile = new OpenFile(table[i].sector);
                Directory *directory = new Directory;
                directory->FetchFrom(openFile);
                directory->Print();
                delete directory;
                delete openFile;
            }
	}
    printf("\n");
    delete hdr;
}

void
Directory::ExpandNamePool()
{
    // allocate new name pool
    int npsz = namePoolSize * 2;
    char *np = new char[npsz];
    memcpy(np, namePool, namePoolSize);
    // replace name pool
    delete [] namePool;
    namePool = np;
    namePoolSize = npsz;
}

void
Directory::ExpandTable()
{
    // allocate new table
    int tsz = tableSize * 2;
    DirectoryEntry *tb = new DirectoryEntry[tsz];
    memcpy(tb, table, tableSize * sizeof(DirectoryEntry));
    // replace table
    delete [] table;
    table = tb;
    tableSize = tsz;
}

void
Directory::ShrinkDirectory()
{
    int tsz = InitialTableSize;
    for (int i = 0; i < tableSize; ++i) {
        if (table[i].inUse) ++tsz;
    }
    DirectoryEntry *oldTable = table;
    char *oldPool = namePool;
    table = new DirectoryEntry[tsz];
    namePool = new char[namePoolSize = InitialPoolSize];
    namePoolFree = 0;
    int j = 0;
    for (int i = 0; i < tsz; ++i) table[i].inUse = FALSE;
    for (int i = 0; i < tableSize; ++i) {
        if (oldTable[i].inUse) {
            ASSERT(j < tsz);
            table[j].inUse = TRUE;
            table[j].sector = oldTable[i].sector;
            table[j].nameOffset = AppendName(oldPool + oldTable[i].nameOffset);
            ++j;
        }
    }
    tableSize = tsz;
    delete [] oldTable;
    delete [] oldPool;
}

int
Directory::AppendName(const char *name)
{
    int pos = namePoolFree;
    int fullLen = strlen(name) + 1; // including zero byte
    while (pos + fullLen > namePoolSize) {
        ExpandNamePool();
    }
    memcpy(namePool + pos, name, fullLen);
    namePoolFree += fullLen;
    return pos;
}

bool
Directory::Empty()
{
    for (int i = 0; i < tableSize; ++i) {
        if (table[i].inUse) return FALSE;
    }
    return TRUE;
}
