// fstest.cc 
//	Simple test routines for the file system.  
//
//	We implement:
//	   Copy -- copy a file from UNIX to Nachos
//	   Print -- cat the contents of a Nachos file 
//	   Perftest -- a stress test for the Nachos file system
//		read and write a really large file in tiny chunks
//		(won't work on baseline system!)
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "utility.h"
#include "filesys.h"
#include "system.h"
#include "thread.h"
#include "disk.h"
#include "stats.h"

#define TransferSize 	10 	// make it small, just to be difficult

//----------------------------------------------------------------------
// Copy
// 	Copy the contents of the UNIX file "from" to the Nachos file "to"
//----------------------------------------------------------------------

void
Copy(char *from, char *to)
{
    FILE *fp;
    OpenFile* openFile;
    int amountRead, fileLength;
    char *buffer;

// Open UNIX file
    if ((fp = fopen(from, "r")) == NULL) {	 
	printf("Copy: couldn't open input file %s\n", from);
	return;
    }

// Figure out length of UNIX file
    fseek(fp, 0, 2);		
    fileLength = ftell(fp);
    fseek(fp, 0, 0);

// Create a Nachos file of the same length
    DEBUG('f', "Copying file %s, size %d, to file %s\n", from, fileLength, to);
    if (!fileSystem->Create(to, fileLength)) {	 // Create Nachos file
	printf("Copy: couldn't create output file %s\n", to);
	fclose(fp);
	return;
    }
    
    openFile = fileSystem->Open(to);
    ASSERT(openFile != NULL);
    
// Copy the data in TransferSize chunks
    buffer = new char[TransferSize];
    while ((amountRead = fread(buffer, sizeof(char), TransferSize, fp)) > 0)
	openFile->Write(buffer, amountRead);	
    delete [] buffer;

// Close the UNIX and the Nachos files
    delete openFile;
    fclose(fp);
}

//----------------------------------------------------------------------
// Print
// 	Print the contents of the Nachos file "name".
//----------------------------------------------------------------------

void
Print(char *name)
{
    OpenFile *openFile;    
    int i, amountRead;
    char *buffer;

    if ((openFile = fileSystem->Open(name)) == NULL) {
	printf("Print: unable to open file %s\n", name);
	return;
    }
    
    buffer = new char[TransferSize];
    while ((amountRead = openFile->Read(buffer, TransferSize)) > 0)
	for (i = 0; i < amountRead; i++)
	    printf("%c", buffer[i]);
    delete [] buffer;

    delete openFile;		// close the Nachos file
    return;
}

//----------------------------------------------------------------------
// PerformanceTest
// 	Stress the Nachos file system by creating a large file, writing
//	it out a bit at a time, reading it back a bit at a time, and then
//	deleting the file.
//
//	Implemented as three separate routines:
//	  FileWrite -- write the file
//	  FileRead -- read the file
//	  PerformanceTest -- overall control, and print out performance #'s
//----------------------------------------------------------------------

#define FileName 	"TestFile"
#define Contents 	"1234567890"
#define ContentSize 	strlen(Contents)
#define FileSize 	((int)(ContentSize * 5000))

static void 
FileWrite()
{
    OpenFile *openFile;    
    int i, numBytes;

    printf("Sequential write of %d byte file, in %d byte chunks\n", 
	FileSize, ContentSize);
    if (!fileSystem->Create(FileName, 0)) {
      printf("Perf test: can't create %s\n", FileName);
      return;
    }
    openFile = fileSystem->Open(FileName);
    if (openFile == NULL) {
	printf("Perf test: unable to open %s\n", FileName);
	return;
    }
    for (i = 0; i < FileSize; i += ContentSize) {
        numBytes = openFile->Write(Contents, ContentSize);
	if (numBytes < 10) {
	    printf("Perf test: unable to write %s\n", FileName);
	    delete openFile;
	    return;
	}
    }
    delete openFile;	// close file
}

static void 
FileRead()
{
    OpenFile *openFile;    
    char *buffer = new char[ContentSize];
    int i, numBytes;

    printf("Sequential read of %d byte file, in %d byte chunks\n", 
	FileSize, ContentSize);

    if ((openFile = fileSystem->Open(FileName)) == NULL) {
	printf("Perf test: unable to open file %s\n", FileName);
	delete [] buffer;
	return;
    }
    for (i = 0; i < FileSize; i += ContentSize) {
        numBytes = openFile->Read(buffer, ContentSize);
	if ((numBytes < 10) || strncmp(buffer, Contents, ContentSize)) {
	    printf("Perf test: unable to read %s\n", FileName);
	    delete openFile;
	    delete [] buffer;
	    return;
	}
    }
    delete [] buffer;
    delete openFile;	// close file
}

void
PerformanceTest()
{
    printf("Starting file system performance test:\n");
    stats->Print();
    FileWrite();
    FileRead();
    if (!fileSystem->Remove(FileName)) {
      printf("Perf test: unable to remove %s\n", FileName);
      return;
    }
    stats->Print();
}

bool FSTest2Ok;

#define FSTest2Chunks 5000
static void FSTest2Create() {
    if (!fileSystem->Create("test2", 0)) {
        printf("FS Test: unable to create file\n");
        FSTest2Ok = false;
    }
}

static void FSTest2Write(char *seq, int len) {
    OpenFile *openFile;
    if ((openFile = fileSystem->Open("test2")) == NULL) {
        printf("FS Test: unable to open file\n");
        FSTest2Ok = false; return;
    }
    for (int i = 0; i < FSTest2Chunks; ++i) {
        if (openFile->Write(seq, len) != len) {
            printf("FS Test: write failed\n");
            FSTest2Ok = false; delete openFile; return;
        }
    }
    delete openFile;
}

static void FSTest2Read(char *s1, char *s2, char *s3, int len) {
    OpenFile *openFile;
    if ((openFile = fileSystem->Open("test2")) == NULL) {
        printf("FS Test: unable to open file\n");
        FSTest2Ok = false; return;
    }
    char *buf = new char[len];
    for (int i = 0; i < FSTest2Chunks; ++i) {
        if (openFile->Read(buf, len) != len) {
            printf("FS Test: read failed\n");
            FSTest2Ok = false; delete []buf; delete openFile; return;
        }
        int p1 = memcmp(buf, s1 ,len);
        int p2 = memcmp(buf, s2 ,len);
        int p3 = memcmp(buf, s3 ,len);
        if (p1 && p2 && p3) {
            printf("FS Test: data corrupted!\n");
            FSTest2Ok = false; delete []buf; delete openFile; return;
        }
    }
    delete [] buf; delete openFile;
}

Barrier *fsTestBarrier;

static void ReadThread(int x) {
    printf("Start Reader...\n");
    FSTest2Read("1234567890","9876543210","aaaaabcdef",10);
    fsTestBarrier->Reach();
}
static void WriteThread(int x) {
    printf("Start Writer [%c]...\n", *(char*)x);
    FSTest2Write((char*)x,10);
    fsTestBarrier->Reach();
}

void RunFSTest2() {
    FSTest2Ok = true;
    FSTest2Create();
    fsTestBarrier = new Barrier("barrier",7);

    FSTest2Write("1234567890",10);
    Thread *t1 = new Thread("w1");
    Thread *t2 = new Thread("w2");
    Thread *t3 = new Thread("w3");
    Thread *t4 = new Thread("r1");
    Thread *t5 = new Thread("r2");
    Thread *t6 = new Thread("r3");
    t1->Fork(WriteThread, (void*)"aaaaabcdef");
    t2->Fork(WriteThread, (void*)"1234567890");
    t3->Fork(WriteThread, (void*)"9876543210");
    t4->Fork(ReadThread, NULL);
    t5->Fork(ReadThread, NULL);
    t6->Fork(ReadThread, NULL);

    fsTestBarrier->Reach();
    if(FSTest2Ok) printf("Passed FS Test!\n");
    fileSystem->Remove("test2");
}

void TryDeleteFile(int x) {
    if (!fileSystem->Remove("test2")) {
        printf("FS Test: unable to remove file\n");
    } else {
        printf("File removed\n");
    }
}

void FileRemoveTest() {
    FSTest2Create();
    FSTest2Write("1234567890",10);

    OpenFile *openFile;
    ASSERT((openFile = fileSystem->Open("test2")) != NULL);
    Thread *t = new Thread("t"); t->Fork(TryDeleteFile,NULL);
    for(int i=0;i<FSTest2Chunks;++i) {
        if(i%2000==0)printf("Progress %d\n",i);
        char s[10]; ASSERT(openFile->Read(s, 10)==10);
        ASSERT(0==memcmp(s,"1234567890",10));
    }
    delete openFile;
}
