// synchdisk.cc 
//	Routines to synchronously access the disk.  The physical disk 
//	is an asynchronous device (disk requests return immediately, and
//	an interrupt happens later on).  This is a layer on top of
//	the disk providing a synchronous interface (requests wait until
//	the request completes).
//
//	Use a semaphore to synchronize the interrupt handlers with the
//	pending requests.  And, because the physical disk can only
//	handle one operation at a time, use a lock to enforce mutual
//	exclusion.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "synchdisk.h"

//----------------------------------------------------------------------
// DiskRequestDone
// 	Disk interrupt handler.  Need this to be a C routine, because 
//	C++ can't handle pointers to member functions.
//----------------------------------------------------------------------

static void
DiskRequestDone (int arg)
{
    SynchDisk* disk = (SynchDisk *)arg;

    disk->RequestDone();
}

//----------------------------------------------------------------------
// SynchDisk::SynchDisk
// 	Initialize the synchronous interface to the physical disk, in turn
//	initializing the physical disk.
//
//	"name" -- UNIX file name to be used as storage for the disk data
//	   (usually, "DISK")
//----------------------------------------------------------------------

SynchDisk::SynchDisk(char* name)
{
    semaphore = new Semaphore("synch disk", 0);
    lock = new Lock("synch disk lock");
    disk = new Disk(name, DiskRequestDone, (int) this);
    bc = new BlockCacheLine[BlockCacheSize];
}

//----------------------------------------------------------------------
// SynchDisk::~SynchDisk
// 	De-allocate data structures needed for the synchronous disk
//	abstraction.
//----------------------------------------------------------------------

SynchDisk::~SynchDisk()
{
    delete disk;
    delete lock;
    delete semaphore;
    delete [] bc;
}

//----------------------------------------------------------------------
// SynchDisk::ReadSector
// 	Read the contents of a disk sector into a buffer.  Return only
//	after the data has been read.
//
//	"sectorNumber" -- the disk sector to read
//	"data" -- the buffer to hold the contents of the disk sector
//----------------------------------------------------------------------

void
SynchDisk::ReadSector(int sectorNumber, char* data)
{
    lock->Acquire();
    int id = GetCacheLine(sectorNumber, TRUE);
    memcpy(data, bc[id].data, SectorSize);
    lock->Release();
}

//----------------------------------------------------------------------
// SynchDisk::WriteSector
// 	Write the contents of a buffer into a disk sector.  Return only
//	after the data has been written.
//
//	"sectorNumber" -- the disk sector to be written
//	"data" -- the new contents of the disk sector
//----------------------------------------------------------------------

void
SynchDisk::WriteSector(int sectorNumber, char* data)
{
    lock->Acquire();
    int id = GetCacheLine(sectorNumber, FALSE);
    memcpy(bc[id].data, data, SectorSize);
    bc[id].dirty = TRUE;
    lock->Release();
}

void
SynchDisk::FlushAllCache() {
    lock->Acquire();
    for (int i = 0; i < BlockCacheSize; ++i) {
        if (bc[i].valid) WriteBackCache(i);
    }
    lock->Release();
}

void
SynchDisk::ReadInternal(int sectorNumber, char* data)
{
    disk->ReadRequest(sectorNumber, data);
    semaphore->P();
}

void
SynchDisk::WriteInternal(int sectorNumber, char* data)
{
    disk->WriteRequest(sectorNumber, data);
    semaphore->P();
}

int
SynchDisk::GetCacheLine(int sectorNumber, bool load)
{
    for(int i = 0; i < BlockCacheSize; ++i) {
        if (bc[i].valid && sectorNumber == bc[i].sector) {
            for (int j = 0; j < BlockCacheSize; ++j)
                if (bc[j].time < bc[i].time)
                    ++bc[j].time;
            bc[i].time = 0; // LRU
            return i;
        }
    }
    return LoadIntoCache(sectorNumber, load);
}

int
SynchDisk::LoadIntoCache(int sectorNumber, bool load)
{
    int t = -1, cid = 0;
    for(int i = 0; i < BlockCacheSize; ++i) {
        if (!bc[i].valid) cid = i, t = BlockCacheSize + 999;
        else if (bc[i].time>t) cid = i, t = ++bc[i].time;
    }
    if (bc[cid].valid) {
        WriteBackCache(cid);
        for (int j = 0; j < BlockCacheSize; ++j)
            if (bc[j].time > bc[cid].time) --bc[j].time;
    }
    bc[cid].sector = sectorNumber;
    bc[cid].time = 0;
    bc[cid].valid = TRUE;
    bc[cid].dirty = FALSE;
    if (load)
        ReadInternal(sectorNumber, bc[cid].data);
    return cid;
}

void
SynchDisk::WriteBackCache(int id)
{
    ASSERT(bc[id].valid);
    if(bc[id].dirty){
        WriteInternal(bc[id].sector, bc[id].data);
        bc[id].dirty = FALSE;
    }
}

//----------------------------------------------------------------------
// SynchDisk::RequestDone
// 	Disk interrupt handler.  Wake up any thread waiting for the disk
//	request to finish.
//----------------------------------------------------------------------

void
SynchDisk::RequestDone()
{ 
    semaphore->V();
}
