// filehdr.cc 
//	Routines for managing the disk file header (in UNIX, this
//	would be called the i-node).
//
//	The file header is used to locate where on disk the 
//	file's data is stored.  We implement this as a fixed size
//	table of pointers -- each entry in the table points to the 
//	disk sector containing that portion of the file data
//	(in other words, there are no indirect or doubly indirect 
//	blocks). The table size is chosen so that the file header
//	will be just big enough to fit in one disk sector, 
//
//      Unlike in a real system, we do not keep track of file permissions, 
//	ownership, last modification date, etc., in the file header. 
//
//	A file header can be initialized in two ways:
//	   for a new file, by modifying the in-memory data structure
//	     to point to the newly allocated data blocks
//	   for a file already on disk, by reading the file header from disk
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"

#include "system.h"
#include "filehdr.h"


static int
calcRequiredSectorNum(int s)
{
    int n = s;
    if (s > NumDirect) {
        s -= NumDirect;
        n += 1 + divRoundUp(s, IndirectSize);
    }
    return n;
}

static void F_DEALLOC(BitMap *bitMap, int *x) {
    ASSERT(bitMap->Test(*x));
    bitMap->Clear(*x);
}

static void F_ALLOC(BitMap *bitMap, int *x) {
    ASSERT(-1 != (*x = bitMap->Find()));
}

//----------------------------------------------------------------------
// FileHeader::Allocate
// 	Initialize a fresh file header for a newly created file.
//	Allocate data blocks for the file out of the map of free disk blocks.
//	Return FALSE if there are not enough free blocks to accomodate
//	the new file.
//
//	"freeMap" is the bit map of free disk sectors
//	"fileSize" is the bit map of free disk sectors
//----------------------------------------------------------------------

bool
FileHeader::Allocate(BitMap *freeMap, int fileSize, FileType fileType)
{ 
    type = fileType;
    numBytes = fileSize;
    numSectors  = divRoundUp(fileSize, SectorSize);
    if (freeMap->NumClear() < calcRequiredSectorNum(numSectors))
	return FALSE;		// not enough space

    for (int i = 0; i < numSectors; i++)
        SetSector(freeMap, i, F_ALLOC);
    return TRUE;
}

bool
FileHeader::EnsureCapacity(BitMap *freeMap, int fileSize)
{
    if (numBytes > fileSize)
        return TRUE;
    int newSectors = divRoundUp(fileSize, SectorSize);
    if (freeMap->NumClear() < 
            calcRequiredSectorNum(newSectors) - 
            calcRequiredSectorNum(numSectors))
        return FALSE;
    numBytes = fileSize;
    while (numSectors < newSectors) {
        SetSector(freeMap, numSectors, F_ALLOC);
        ++numSectors;
    }
    return TRUE;
}

//----------------------------------------------------------------------
// FileHeader::Deallocate
// 	De-allocate all the space allocated for data blocks for this file.
//
//	"freeMap" is the bit map of free disk sectors
//----------------------------------------------------------------------

void 
FileHeader::Deallocate(BitMap *freeMap)
{
    for (int i = 0; i < numSectors; ++i)
        SetSector(freeMap, i, F_DEALLOC);
}

//----------------------------------------------------------------------
// FileHeader::FetchFrom
// 	Fetch contents of file header from disk. 
//
//	"sector" is the disk sector containing the file header
//----------------------------------------------------------------------

void
FileHeader::FetchFrom(int sector)
{
    synchDisk->ReadSector(sector, (char *)this);
    int k = numSectors;
    if (k > NumDirect) {
        k -= NumDirect;
        im1.FetchFrom(indirect);
        for (int i = 0; i < IndirectSize; ++i) {
            if (k <= 0) break;
            im2[i].FetchFrom(im1.sector[i]);
            k -= IndirectSize;
        }
        ASSERT(k <= 0);
    }
}

//----------------------------------------------------------------------
// FileHeader::WriteBack
// 	Write the modified contents of the file header back to disk. 
//
//	"sector" is the disk sector to contain the file header
//----------------------------------------------------------------------

void
FileHeader::WriteBack(int sector)
{
    ASSERT(sizeof(FileHeader) - sizeof(im1) - sizeof(im2) <= SectorSize);
    synchDisk->WriteSector(sector, (char *)this); 
    int k = numSectors;
    if (k > NumDirect) {
        k -= NumDirect;
        im1.WriteBack(indirect);
        for (int i = 0; i < IndirectSize; ++i) {
            if (k <= 0) break;
            im2[i].WriteBack(im1.sector[i]);
            k -= IndirectSize;
        }
        ASSERT(k <= 0);
    }
}

//----------------------------------------------------------------------
// FileHeader::ByteToSector
// 	Return which disk sector is storing a particular byte within the file.
//      This is essentially a translation from a virtual address (the
//	offset in the file) to a physical address (the sector where the
//	data at the offset is stored).
//
//	"offset" is the location within the file of the byte in question
//----------------------------------------------------------------------

int
FileHeader::ByteToSector(int offset)
{
    return(GetSector(offset / SectorSize));
}

//----------------------------------------------------------------------
// FileHeader::FileLength
// 	Return the number of bytes in the file.
//----------------------------------------------------------------------

int
FileHeader::FileLength()
{
    return numBytes;
}

//----------------------------------------------------------------------
// FileHeader::Print
// 	Print the contents of the file header, and the contents of all
//	the data blocks pointed to by the file header.
//----------------------------------------------------------------------

void
FileHeader::Print()
{
    int i, j, k;
    char *data = new char[SectorSize];

    printf("FileHeader contents. Type: %s  File size: %d.  File blocks:\n",
            type == NORMAL_FILE ? "normal" : "directory",
            numBytes);
    for (i = 0; i < numSectors; i++)
	printf("%d ", GetSector(i));
    printf("\nIndirect map blocks:\n");
    if (numSectors > NumDirect) {
        int c = divRoundUp(numSectors - NumDirect, IndirectSize);
        printf("%d ", indirect);
        for (i = 0; i < c; ++i) 
            printf("%d ", im1.sector[i]);
    }
    printf("\nFile contents:\n");
    for (i = k = 0; i < numSectors; i++) {
	synchDisk->ReadSector(GetSector(i), data);
        for (j = 0; (j < SectorSize) && (k < numBytes); j++, k++) {
	    if ('\040' <= data[j] && data[j] <= '\176')   // isprint(data[j])
		printf("%c", data[j]);
            else
		printf("\\%x", (unsigned char)data[j]);
	}
        printf("\n"); 
    }
    delete [] data;
}

//----------------------------------------------------------------------
// FileHeader::GetFileType
//      Get file type
//----------------------------------------------------------------------

FileType
FileHeader::GetFileType()
{
    return type;
}

void
FileHeader::SetSector(BitMap *bitMap, int id, void (*func)(BitMap*, int*))
{
    if (id < NumDirect) {
        func(bitMap, &(directMap[id]));
    } else {
        id -= NumDirect;
        if (id == 0)
            func(bitMap, &indirect);
        int ix = id / IndirectSize;
        id %= IndirectSize;
        ASSERT(ix < IndirectSize);
        if (id == 0) 
            func(bitMap, &(im1.sector[ix]));
        func(bitMap, &(im2[ix].sector[id]));
    }
}

int
FileHeader::GetSector(int id) {
    if (id < NumDirect)
        return directMap[id];
    id -= NumDirect;
    return im2[id / IndirectSize].sector[id % IndirectSize];
}

void
IndirectMap::FetchFrom(int sectorNumber)
{
    synchDisk->ReadSector(sectorNumber, (char *)this);
}

void
IndirectMap::WriteBack(int sectorNumber)
{
    ASSERT(sizeof(IndirectMap) <= SectorSize);
    synchDisk->WriteSector(sectorNumber, (char *)this);
}
