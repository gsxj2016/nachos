#include "syscall.h"

int
main()
{
    char *data = "hello, world!";
    char buf[5];
    int len = 13;
    int rd, i;
    OpenFileId fd;
    Create("0.txt");

    fd = Open("0.txt");
    Write(data, len, fd);
    Close(fd);

    fd = Open("0.txt");
    rd = Read(buf, 5, fd);
    if (rd != 5) Exit(1);
    for (i = 0; i < rd; ++i) if(buf[i] != data[i]) Exit(2);
    data += rd;
    rd = Read(buf, 5, fd);
    if (rd != 5) Exit(1);
    for (i = 0; i < rd; ++i) if(buf[i] != data[i]) Exit(2);
    data += rd;
    rd = Read(buf, 5, fd);
    if (rd != 3) Exit(1);
    for (i = 0; i < rd; ++i) if(buf[i] != data[i]) Exit(2);
    Exit(0);
}
