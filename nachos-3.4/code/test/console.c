#include "syscall.h"

int
main()
{
    char buf[4];
    for (;;) {
        Read(buf, 4, ConsoleInput);
        if (buf[0] == 'e' && buf[1] == 'x' && buf[2] == 'i' && buf[3] =='t') {
            Halt();
        }
        Write("==Quad==\n", 9, ConsoleOutput);
        Write(buf, 4, ConsoleOutput);
        Write("\n========\n", 10, ConsoleOutput);
    }
}
