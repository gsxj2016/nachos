/* sort_lite.c 
 *    Test program to sort a large number of integers.
 */

#include "syscall.h"

int A[256];

int
main()
{
    int i, j, tmp;

    /* first initialize the array, in reverse sorted order */
    for (i = 0; i < 256; i++)		
        A[i] = 256 - i;

    /* then sort! */
    for (i = 0; i < 255; i++)
        for (j = i; j < (255 - i); j++)
	   if (A[j] > A[j + 1]) {	/* out of order -> need to swap ! */
	      tmp = A[j];
	      A[j] = A[j + 1];
	      A[j + 1] = tmp;
    	   }
    Halt();
}
