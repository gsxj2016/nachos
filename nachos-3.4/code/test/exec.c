#include "syscall.h"

int
main()
{
    SpaceId id;
    int ec;
    Exec("test/file");
    id = Exec("test/sort");
    ec = Join(id);
    Exec("test/sort");
    Exit(ec + 1);
}
