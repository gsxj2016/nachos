#include "syscall.h"

int state;

void work_thread() {
    state = 1;
}

int main()
{
    state = 0;
    Fork(work_thread);
    while (state != 1) {
        Yield();
    }
    Exit(state);
}
